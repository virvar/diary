(ns diary.core
  (:require [enfocus.core :as ef]
            [enfocus.events :as ev]
            [cljs-time.core :as time]
            [cljs-time.format :as time-format]
            [diary.loader :as loader])
  (:require-macros [enfocus.macros :as em]))

(declare start)

(declare show-edit-events-form)

(defn error-handler
  [{:keys [status status-text]}]
  (.log js/console (str "Something bad happened: " status-text)))

(em/defsnippet modal-dialog :compiled "templates/modal.html" "#modal-dialog" [])

(defn event-type-deleted
  []
  (show-edit-events-form))

(defn delete-event-type-btn-click-handler
  [event-type]
  (fn [] (loader/delete-event-type (:id event-type) event-type-deleted error-handler)))

(defn set-delete-event-type-content
  [event-type]
  (ef/at ".modal-header" (ef/content "Delete"))
  (ef/at ".modal-body" (ef/content (str "Delete " (:name event-type) "?")))
  (ef/at "#dialog-confirm" (ef/do-> (ef/content "Delete")
                                    (ev/remove-listeners :click)
                                    (ev/listen :click (delete-event-type-btn-click-handler event-type)))))

(em/defsnippet btn-delete-event-type :compiled "templates/modal.html" "#modal-btn"
  [event-type]
  "*"
  (ef/do-> (ef/remove-attr :id)
           (ef/set-attr :data-id (:id event-type))
           (ef/content "Delete")
           (ev/listen :click #(set-delete-event-type-content event-type))))

(em/deftemplate edit-event-types :compiled "templates/event-types.html"
  [event-types]
  "#event-types-edit table tbody > tr:first-child"
  (em/clone-for [event-type event-types]
                "> :nth-child(1)" (ef/content (:name event-type))
                "> :nth-child(2)" (ef/content (btn-delete-event-type event-type))))

(defn event-types-for-edit-loaded
  [data]
  (.log js/console (str data))
  (ef/at ".container" (ef/append (edit-event-types data))))

(defn show-edit-events-form
  []
  (.log js/console "show-edit-events-form")
  (ef/at ".container"
         (ef/content (modal-dialog)))
  (loader/load-event-types event-types-for-edit-loaded error-handler))

(em/defsnippet header :compiled "templates/template.html" "#header" [])

(em/defsnippet new-event :compiled "templates/template.html" "#new-event"
  []
  "#event-date" (ef/set-attr
                 :value
                 (time-format/unparse (time-format/formatter "yyyy/MM/dd") (time/now)))
  "#show-edit-event-types" (ev/listen :click show-edit-events-form))

(em/defsnippet event-types-select :compiled "templates/template.html" "#event-type-id"
  [event-types]
  "#event-type-id-option"
  (em/clone-for [event-type event-types]
                (ef/do-> (ef/remove-attr :id)
                         (ef/set-attr :value (:id event-type))
                         (ef/content (:name event-type)))))

(em/defsnippet events-blank :compiled "templates/template.html" "#events" [])

(defn event-deleted
  []
  (start))

(defn delete-event-btn-click-handler
  [event]
  (fn [] (loader/delete-event (:id event) event-deleted error-handler)))

(defn set-delete-modal-content
  [event]
  (ef/at ".modal-header" (ef/content "Delete"))
  (ef/at ".modal-body" (ef/content (str "Delete " (:name event) "?")))
  (ef/at "#dialog-confirm" (ef/do-> (ef/content "Delete")
                                    (ev/remove-listeners :click)
                                    (ev/listen :click (delete-event-btn-click-handler event)))))

(em/defsnippet modal-button :compiled "templates/modal.html" "#modal-btn" [event]
  "*"
  (ef/do-> (ef/remove-attr :id)
           (ef/set-attr :data-id (:id event))
           (ef/content "Delete")
           (ev/listen :click #(set-delete-modal-content event))))

(em/defsnippet events-block :compiled "templates/template.html" "#events"
  [events]
  "#events table tbody > tr:first-child"
  (em/clone-for [event events]
                "> :nth-child(1)" (ef/content (time-format/unparse
                                               (time-format/formatter "yyyy/MM/dd")
                                               (time/date-time (:event_date event))))
                "> :nth-child(2)" (ef/content (:name event))
                "> :nth-child(3)" (ef/content (modal-button event))))

(defn event-added
  [response]
  (start))

(defn ^:export add-event
  []
  (.log js/console (ef/from "#event-date" (ef/read-form-input)))
  (.log js/console (ef/from "#event-type-id" (ef/read-form-input)))
  (.log js/console (ef/from "#new-event-type-name" (ef/read-form-input)))
  (.log js/console (ef/from "#comment" (ef/read-form-input)))
  (loader/add-event {:event-date (ef/from "#event-date" (ef/read-form-input))
                     :event-type-id (ef/from "#event-type-id" (ef/read-form-input))
                     :new-event-type-name (ef/from "#new-event-type-name" (ef/read-form-input))
                     :comment (ef/from "#comment" (ef/read-form-input))}
                    event-added
                    error-handler))

(defn event-types-loaded
  [data]
  (.log js/console (str data))
  (ef/at "#event-type-id" (ef/substitute (event-types-select data))))

(defn events-loaded
  [data]
  (.log js/console (str data))
  (ef/at "#events" (ef/substitute (events-block data))))

(defn start
  []
  (ef/at ".container"
         (ef/do-> (ef/content (header))
                  (ef/append (modal-dialog))
                  (ef/append (new-event))
                  (ef/append (events-blank))))
  (loader/load-event-types event-types-loaded error-handler)
  (loader/load-events events-loaded error-handler))

(set! (.-onload js/window) #(em/wait-for-load (start)))
