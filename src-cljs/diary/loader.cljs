(ns diary.loader
  (:require [enfocus.core :as ef]
            [enfocus.events :as ev]
            [ajax.core :refer [GET POST]]
            [cljs-time.core :as time]
            [cljs-time.format :as time-format])
  (:require-macros [enfocus.macros :as em]))

;;; event-types
(defn load-event-types
  [handler error-handler]
  (GET "/event-types"
       {:handler handler
        :error-handler error-handler}))

(defn delete-event-type
  [event-type-id handler error-handler]
  (POST "event-types/delete-event-type"
        {:format :edn
         :params {:event-type-id event-type-id}
         :handler handler
         :error-handler error-handler}))

;;; events
(defn load-events
  [handler error-handler]
  (GET "/events"
       {:handler handler
        :error-handler error-handler}))

(defn add-event
  [params handler error-handler]
  (POST "/events/add"
        {:format :edn
         :params params
         :handler handler
         :error-handler error-handler}))

(defn delete-event
  [event-id handler error-handler]
  (POST "/events/delete-event"
        {:format :edn
         :params {:event-id event-id}
         :handler handler
         :error-handler error-handler}))
