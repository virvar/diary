(defproject diary "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src-clj"]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2371"
                  :exclusions [org.apache.ant/ant]]
                 [com.andrewmcveigh/cljs-time "0.2.4"]
                 [enfocus "2.1.1"]
                 [cljs-ajax "0.3.3"]
                 [compojure "1.3.1"]
                 [sonian/carica "1.1.0"]
                 [korma "0.4.0"]
                 [mysql/mysql-connector-java "5.1.25"]
                 [fogus/ring-edn "0.2.0"]]
  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-ring "0.8.13"]]
  :cljsbuild
  {:builds
   [{:source-paths ["src-cljs"]
     :compiler {:output-to "resources/public/js/main.js"
                :optimizations :whitespace
                :pretty-print true}}]}
  :ring {:handler diary.routes/app}
  :jvm-opts ["-Duser.timezone=UTC"])
