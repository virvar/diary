(ns diary.database
  (:require [korma.db :refer [defdb]]
            [korma.core :refer :all])
  (:use [carica.core]))

(defdb db (config :db))

(defentity event-types
  (table :event_types))

(defentity events
  (table :events)
  (belongs-to event-types {:fk :event_type_id}))

(defn get-last-event-types
  []
  (select event-types
          (fields :id
                  :name)
          (join events)
          (aggregate (max :events.event_date) :last-date :id)
          (order :last-date :desc)
          (order :id :desc)))

(defn add-event-type
  [name]
  (insert event-types
          (values {:name name})))

(defn delete-event-type
  [event-type-id]
  (delete event-types
          (where {:id event-type-id})))

(defn get-events
  []
  (select events
          (fields :id
                  :event_date)
          (with event-types
                (fields :name))
          (order :event_date :desc)
          (order :id :desc)))

(defn get-events-days
  []
  (select events
          (aggregate (count :*) :days :event_type_id)))

(defn add-event
  [event-type-id date comment]
  (insert events
          (values {:event_type_id event-type-id
                   :event_date date
                   :comment comment})))

(defn delete-event
  [event-id]
  (delete events
          (where {:id event-id})))
