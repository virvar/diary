(ns diary.routes
  (:use [compojure.core]
        [compojure.handler]
        [ring.middleware.edn]
        [carica.core])
  (:require [clojure.java.io :as io]
            [diary.database :as db]
            [compojure.route :as route]))

(defn response
  [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/edn;charset=utf-8"}
   :body (pr-str data)})

(defn delete-event-type
  [event-type-id]
  (db/delete-event-type event-type-id)
  (response nil))

(defroutes event-types-routes
  (GET "/" [] (response (db/get-last-event-types)))
  (POST "/delete-event-type" [event-type-id] (delete-event-type event-type-id)))

(defn get-events
  []
  (response (db/get-events)))

(defn add-event
  [event-date event-type-id new-event-type-name comment]
  (if (if (not (clojure.string/blank? new-event-type-name))
        (when-let [insert-result (db/add-event-type new-event-type-name)]
          (let [new-event-type-id (:generated_key insert-result)]
            (db/add-event new-event-type-id event-date comment)))
        (db/add-event event-type-id event-date comment))
    (response nil)
    (response nil 420)))

(defn delete-event
  [event-id]
  (db/delete-event event-id)
  (response nil))

(defroutes events-routes
  (GET "/" [] (get-events))
  (POST "/add" [event-date event-type-id new-event-type-name comment]
        (add-event event-date event-type-id new-event-type-name comment))
  (POST "/delete-event" [event-id] (delete-event event-id)))

(defroutes compojure-handler
  (GET "/" [] (slurp (io/resource "public/html/index.html")))
  (context "/event-types" [] event-types-routes)
  (context "/events" [] events-routes)
  (route/resources "/")
  (route/not-found "<h3>page not found</h3>"))

(def app
  (-> compojure-handler
      (site)
      (wrap-edn-params)))

;(defonce server (ring.adapter.jetty/run-jetty #'app {:port 8080 :join? false}))

;(.stop server)

;(.start server)
