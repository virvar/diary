create database diary;
grant all on diary.* to diaryuser@localhost identified by '1';
create table diary.event_types(id int auto_increment primary key, name varchar(1024) not null);
create table diary.events(id int auto_increment primary key, event_type_id int not null, event_date date not null, comment text);
